const express = require('express');
const https = require('https');
const sspi = require('node-sspi');
const cors = require('cors');
const fs = require('fs');
const controller = require('./wauth-controller');

const app = express();

const nsspi = new sspi({
    retrieveGroups: true
});

app.use(cors());

app.use('/', (req, res, next) => {

    nsspi.authenticate(req, res, (err) => {
        res.finished || next();
    });
});

app.use('/', controller);

var options = {
    key: fs.readFileSync('./cert/private.key'),
    passphrase: 'shuttle12',
    cert: fs.readFileSync('./cert/public.cert')
};

const port = process.env.PORT || 4430;

https.createServer(options, app).listen(port, () => {
    console.log('WAuth started on port ' + port);
});


