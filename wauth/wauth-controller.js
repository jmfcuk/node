const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');

router.use(bodyParser.json({ extended: true }));

router.get('/', (req, res) => {

    let groups = [];

    if (req.connection.userGroups) {
        for (var i in req.connection.userGroups) {
            groups.push(req.connection.userGroups[i]);
        }
    };

    const resp = {
        name: req.connection.user,
        ssid: req.connection.userSid,
        groups: groups
    };

    res.status(200).send(resp);
});

module.exports = router;
