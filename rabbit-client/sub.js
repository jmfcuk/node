var q = 'tasks';
 
var open = require('amqplib').connect('amqp://admin:admin@localhost');
 
open.then(function(conn) {
    return conn.createChannel();
  }).then(function(ch) {
    return ch.assertQueue(q).then(function(ok) {
      return ch.consume(q, function(msg) {
        if (msg !== null) {
          console.log('Received : ' + msg.content.toString());
          ch.ack(msg);
        }
      });
    });
  }).catch(console.warn);
