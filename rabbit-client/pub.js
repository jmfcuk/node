var q = 'tasks';
 
var open = require('amqplib').connect('amqp://admin:admin@localhost');
 
open.then(function(conn) {
  return conn.createChannel();
}).then(function(ch) {
  return ch.assertQueue(q).then(function(ok) {
    console.log('sending some message');
    return ch.sendToQueue(q, new Buffer('some message'));
  });
}).catch(console.warn);

